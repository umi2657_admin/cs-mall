package com.cskaoyan.gateway.controller;

import com.mall.user.UserTestService;
import com.mall.user.annotation.Anoymous;
import org.apache.dubbo.config.annotation.Reference;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@Anoymous
public class Testontroller {

    @Reference
    UserTestService testService;

    @RequestMapping("test")
    public String test() {
        String nameById = testService.getNameById(71);
        return nameById;
    }
}
