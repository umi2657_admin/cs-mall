package com.mall.user.services;

import com.mall.user.UserTestService;
import com.mall.user.dal.entitys.Member;
import com.mall.user.dal.persistence.MemberMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component
@org.apache.dubbo.config.annotation.Service(interfaceClass = UserTestService.class)
public class UserTestServiceImpl implements UserTestService {
    @Autowired
    MemberMapper memberMapper;
    @Override
    public String getNameById(Integer id) {
        Member member = memberMapper.selectByPrimaryKey(id);;
        return member.getUsername();

    }
}
